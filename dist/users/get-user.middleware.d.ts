import { NestMiddleware } from '@nestjs/common';
export declare class GetUserMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: () => void): void;
}
