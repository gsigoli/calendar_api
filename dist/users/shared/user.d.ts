import { Document } from 'mongoose';
export declare class User extends Document {
    email: string;
    password: string;
}
