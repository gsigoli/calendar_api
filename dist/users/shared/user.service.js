"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const password = require("password-hash-and-salt");
const jwt = require("jsonwebtoken");
const constants_1 = require("../../constants");
let UserService = class UserService {
    constructor(userModel) {
        this.userModel = userModel;
    }
    async login(user) {
        const findUser = await this.userModel.findOne({ email: user.email });
        if (!findUser) {
            throw new common_1.UnauthorizedException();
        }
        return new Promise((resolve, reject) => {
            password(user.password).verifyAgainst(findUser.password, (error, verified) => {
                if (!verified) {
                    reject(new common_1.UnauthorizedException());
                }
                const authJwtToken = jwt.sign({ email: user.email, _id: findUser._id }, constants_1.JWT_SECRET);
                resolve({ authJwtToken });
            });
        });
    }
    async register(user) {
        const createUser = new this.userModel(user);
        await password(createUser.password).hash(async (error, hash) => {
            if (error) {
                throw new Error(error);
            }
            createUser.password = hash;
            return await createUser.save();
        });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('User')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map