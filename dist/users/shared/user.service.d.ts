import { Model } from 'mongoose';
import { User } from './user';
export declare class UserService {
    private readonly userModel;
    constructor(userModel: Model<User>);
    login(user: User): Promise<unknown>;
    register(user: User): Promise<void>;
}
