import { UserService } from './shared/user.service';
import { User } from './shared/user';
export declare class UsersController {
    private userService;
    constructor(userService: UserService);
    login(user: User): Promise<unknown>;
    register(user: User): Promise<void>;
}
