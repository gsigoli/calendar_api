/// <reference types="mongodb" />
import { Event } from './event';
import { Model } from 'mongoose';
export declare class EventService {
    private readonly eventModel;
    constructor(eventModel: Model<Event>);
    getAll(userId: string): Promise<Event[]>;
    getById(id: string): Promise<Event>;
    create(event: Event, userId: string): Promise<Event>;
    update(id: string, event: Event): Promise<Event>;
    delete(id: string): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    searchDate(userId: String, event: Event): Promise<Event[]>;
}
