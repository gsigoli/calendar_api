"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let EventService = class EventService {
    constructor(eventModel) {
        this.eventModel = eventModel;
    }
    async getAll(userId) {
        return await this.eventModel.find({ user_id: userId }).exec();
    }
    async getById(id) {
        return await this.eventModel.findOne({ _id: id }).exec();
    }
    async create(event, userId) {
        event.user_id = userId;
        const createEvent = new this.eventModel(event);
        return await createEvent.save();
    }
    async update(id, event) {
        await this.eventModel.updateOne({ _id: id }, event).exec();
        return this.getById(id);
    }
    async delete(id) {
        return await this.eventModel.deleteOne({ _id: id }).exec();
    }
    async searchDate(userId, event) {
        return await this.eventModel.find({ $and: [{ user_id: userId },
                { $or: [{ initialDate: event.initialDate }, { finalDate: event.finalDate }] }
            ] }).exec();
    }
};
EventService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Event')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], EventService);
exports.EventService = EventService;
//# sourceMappingURL=event.service.js.map