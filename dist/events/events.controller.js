"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const authentication_guard_1 = require("./../guards/authentication.guard");
const event_service_1 = require("./shared/event.service");
const common_1 = require("@nestjs/common");
const event_1 = require("./shared/event");
const T_FORMAT = 'HHmm';
let EventsController = class EventsController {
    constructor(eventService) {
        this.eventService = eventService;
    }
    async getAll(request) {
        const userId = request['user']._id;
        return this.eventService.getAll(userId);
    }
    async getById(id) {
        return this.eventService.getById(id);
    }
    async create(event, request) {
        const userId = request['user']._id;
        const overwrite = await this.checkOverwrite(userId, event);
        if (overwrite) {
            throw new common_1.HttpException('Overwrite', 400);
        }
        return this.eventService.create(event, userId);
    }
    async update(id, event) {
        return this.eventService.update(id, event);
    }
    async delete(id) {
        return this.eventService.delete(id);
    }
    async checkOverwrite(userId, event) {
        let events = await this.eventService.searchDate(userId, event);
        const startTime = event.startTime;
        const endTime = event.endTime;
        if (events.length > 0) {
            let overwrite = false;
            events.forEach(e => {
                if (e.startTime > startTime && e.finalDate == event.finalDate && endTime < e.startTime) {
                    overwrite = false;
                    return;
                }
                else {
                    overwrite = true;
                    return;
                }
            });
            return overwrite;
        }
        return false;
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "getAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "getById", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Request()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [event_1.Event, Object]),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, event_1.Event]),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], EventsController.prototype, "delete", null);
EventsController = __decorate([
    common_1.Controller('event'),
    common_1.UseGuards(authentication_guard_1.AuthenticationGuard),
    __metadata("design:paramtypes", [event_service_1.EventService])
], EventsController);
exports.EventsController = EventsController;
//# sourceMappingURL=events.controller.js.map