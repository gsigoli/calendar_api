"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.EventSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
    },
    initialDate: String,
    finalDate: String,
    startTime: String,
    endTime: String,
    user_id: String,
});
//# sourceMappingURL=event.schema.js.map