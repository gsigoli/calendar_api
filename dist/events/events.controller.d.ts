/// <reference types="mongodb" />
/// <reference types="mongoose" />
import { EventService } from './shared/event.service';
import { Event } from './shared/event';
export declare class EventsController {
    private eventService;
    constructor(eventService: EventService);
    getAll(request: any): Promise<Event[]>;
    getById(id: string): Promise<Event>;
    create(event: Event, request: any): Promise<Event>;
    update(id: string, event: Event): Promise<Event>;
    delete(id: string): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    checkOverwrite(userId: any, event: Event): Promise<boolean>;
}
