import { Injectable } from '@nestjs/common';
import { Event } from './event';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class EventService {
    constructor(@InjectModel('Event') private readonly eventModel: Model<Event>) { }

    async getAll(userId: string) {
        return await this.eventModel.find({ user_id: userId }).exec();
    }

    async getById(id: string) {
        return await this.eventModel.findOne({ _id: id }).exec();
    }

    async create(event: Event, userId: string) {
        event.user_id = userId;
        const createEvent = new this.eventModel(event);
        return await createEvent.save();
    }

    async update(id: string, event: Event) {
        await this.eventModel.updateOne({ _id: id }, event).exec();
        return this.getById(id);
    }

    async delete(id: string) {
        return await this.eventModel.deleteOne({ _id: id }).exec();
    }

    async searchEventsbyDate(userId: String, initialDate: string, finalDate?: string) {
        return await this.eventModel.find({
            $and: [{ user_id: userId },
            { $or: [{ initialDate: initialDate }, { finalDate: finalDate }] }
            ]
        }).exec();
    }
}
