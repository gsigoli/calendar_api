import { Document } from 'mongoose';
export class Event extends Document {
    description: string;
    initialDate: string;
    finalDate: string;
    startTime: string;
    endTime: string;
    user_id: string;
}
