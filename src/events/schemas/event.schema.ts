import * as mongoose from 'mongoose';

export const EventSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
    },
    initialDate: String,
    finalDate: String,
    startTime: String,
    endTime: String,
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    linked_users_ids : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    isAdmin: Boolean
})
