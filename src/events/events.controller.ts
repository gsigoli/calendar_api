import { AuthenticationGuard } from './../guards/authentication.guard';
import { EventService } from './shared/event.service';
import { Controller, Get, Param, Post, Body, Put, Delete, Request, UseGuards, HttpException } from '@nestjs/common';
import { Event } from './shared/event';
import * as moment from 'moment';

const T_FORMAT = 'HHmm'

@Controller('event')
@UseGuards(AuthenticationGuard)
export class EventsController {

    constructor(private eventService: EventService) { }

    @Get()
    async getAll(@Request() request: any): Promise<Event[]> {
        const userId = request['user']._id;
        return this.eventService.getAll(userId);
    }

    @Get(':id')
    async getById(@Param('id') id: string): Promise<Event> {
        return this.eventService.getById(id);
    }

    @Post()
    async create(@Body() event: Event, @Request() request: any): Promise<Event> {
        const userId = request['user']._id;
        const overwrite = await this.checkOverwrite(userId, event);
        if (overwrite) {
            throw new HttpException('Overwrite', 400);
        }
        return this.eventService.create(event, userId);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() event: Event): Promise<Event> {
        return this.eventService.update(id, event);
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return this.eventService.delete(id);
    }

    async checkOverwrite(userId, event: Event): Promise<boolean> {
        let events = await this.eventService.searchEventsbyDate(userId, event.initialDate, event.finalDate);
        const startTime = event.startTime;
        const endTime = event.endTime;
        if (events.length > 0) {
            let overwrite = false;
            events.forEach(e => {
                if ((e.startTime > startTime && e.finalDate == event.finalDate && endTime < e.startTime) /*verifica se o evento a ser cadastrado é antes do horário dos eventos já criados*/
                    || (startTime > e.startTime && startTime > e.endTime && e.finalDate == event.finalDate && endTime > e.endTime)) /*verifica se o evento a ser cadastrado é depois do horário dos eventos já criados*/ {
                    overwrite = false;
                    return
                }
                overwrite = true;
                return
            })
            return overwrite;
        }
        return false;
    }
}
