import { EventsController } from './events/events.controller';
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { DB_CONNECTION } from './constants';
import { EventsModule } from './events/events.module';
import { GetUserMiddleware } from './middleware/get-user.middleware'

@Module({
  imports: [
    MongooseModule.forRoot(DB_CONNECTION),
    UsersModule,
    EventsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule{

  configure(consumer: MiddlewareConsumer): void {

    consumer.apply(GetUserMiddleware)
    .forRoutes(
      EventsController
    )

  }

}
