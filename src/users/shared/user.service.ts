import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './user';

import * as password from 'password-hash-and-salt';
import * as jwt from 'jsonwebtoken';
import { JWT_SECRET } from 'src/constants';

@Injectable()
export class UserService {

    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    async login(user: User) {
        const findUser = await this.userModel.findOne({ email: user.email });
        if (!findUser) {
            throw new UnauthorizedException();
        }
        return new Promise((resolve, reject) => {
            password(user.password).verifyAgainst(
                findUser.password,
                (error, verified) => {
                    if (!verified) {
                        reject(new UnauthorizedException());
                    }
                    const authJwtToken = jwt.sign({ email: user.email, _id: findUser._id }, JWT_SECRET);
                    resolve({ authJwtToken });
                }
            )
        })
    }

    async register(user: User) {
        const createUser = new this.userModel(user);
        await password(createUser.password).hash(async (error, hash) => {
            if (error) {
                throw new Error(error);
            }
            createUser.password = hash;
            return await createUser.save();
        })
    }
}
