import { Controller, Post, Body } from '@nestjs/common';

import { UserService } from './shared/user.service'
import { User } from './shared/user'

@Controller()
export class UsersController {

    constructor(private userService: UserService) { }

    @Post('/login')
    async login(@Body() user: User) {
        return this.userService.login(user);
    }

    @Post('/register')
    async register(@Body() user: User) {
        return this.userService.register(user);
    }

}
