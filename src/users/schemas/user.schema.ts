import * as mongoose from 'mongoose';
import { EventSchema } from '../../events/schemas/event.schema'

export const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
})
